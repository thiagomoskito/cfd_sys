<!doctype html>
<html lang="en">

<head>
<title>CFD Business - Leads </title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="Sys Template">
<meta name="author" content="A42, design by: Thiago Moskito">

<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../assets/vendor/animate-css/animate.min.css">
<link rel="stylesheet" href="../assets/vendor/font-awesome/css/font-awesome.min.css">

<link rel="stylesheet" href="../assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="../assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css">
<link rel="stylesheet" href="../assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css">
<link rel="stylesheet" href="../assets/vendor/sweetalert/sweetalert.css"/>

<link rel="stylesheet" href="assets/css/introjs.css">

<!-- MAIN CSS -->
<link rel="stylesheet" href="assets/css/main.css">
<link rel="stylesheet" href="assets/css/cfd_skins.css">
<style>
    td.details-control {
    background: url('../assets/images/details_open.png') no-repeat center center;
    cursor: pointer;
}
    tr.shown td.details-control {
        background: url('../assets/images/details_close.png') no-repeat center center;
    }
</style>
<script src="intro.js"></script>
<script type="text/javascript">
  var autostartMatch = window.location.search.match(/(?:\?|\&)autostart=([^\&]*)(?:\&|$)/);
  if (autostartMatch && autostartMatch[1]) {
    jQuery(function() {
      introJs().start();
    })
  }
</script>
</head>
<body class="theme-cfd">

<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img src="../assets/images/thumbnail_dark.png" width="50" height="50" alt="CFD Business"></div>
        <p>Carregando...</p>
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay" style="display: none;"></div>

<div id="wrapper">

    <nav class="navbar navbar-fixed-top">
        <div class="container-fluid">

            <div class="navbar-brand">
                <a href="cfd_painel.html">
                    <img src="../assets/images/logo-icon.svg" alt="Logo" class="img-responsive logo">
                    <span class="name"><img src="../assets/images/lg_60.png" alt="Logo"></span>
                </a>
            </div>
            
            <?php
            include('cfd_top.php');
            ?>

        </div>
    </nav>

    <?php
    include('cfd_menu.php');
    ?>

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">
                        <h2>Leads <span>administração</span></h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="cfd_painel.php"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item active">Leads</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header" data-intro="Lorem ipsum dolor sit amet" data-step="1">
                            <a class="btn btn-outline-warning" href="#NovoLead" data-toggle="modal" data-target="#NovoLead">Novo Lead</a>
                            <button type="button" class="btn btn-outline-warning js-sweetalert" data-type="success">Gerar Leads aos Downlines</button>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover js-basic-example dataTable table-custom">
                                    <thead>
                                        <tr>
                                            <th data-intro="Clique nas setas para ordenar as colunas" data-step="2">Name</th>
                                            <th>Email</th>
                                            <th>Turno para contato</th>
                                            <th>Telefone</th>
                                            <th>Status</th>
                                            <th>Último follow up</th>
                                            <th>Ações</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Prof. Chasity Monahan</td>
                                            <td>dominic.gottlieb@example.com</td>
                                            <td>Vespertino</td>
                                            <td>61 99876-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Ashton Gusmão</td>
                                            <td>Junior Technical Author</td>
                                            <td>Noturno</td>
                                            <td>61 98745-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Mariano Romero</td>
                                            <td>joaquim@gmail.com</td>
                                            <td>Matutino</td>
                                            <td>61 99876-5642</td>
                                            <td>Agendado</td>
                                            <td>02-02-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Oliver Monahan</td>
                                            <td>dominic.gottlieb@example.com</td>
                                            <td>Vespertino</td>
                                            <td>61 99876-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Niel Silva</td>
                                            <td>Junior Technical Author</td>
                                            <td>Noturno</td>
                                            <td>61 98745-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Nando Romero</td>
                                            <td>joaquim@gmail.com</td>
                                            <td>Matutino</td>
                                            <td>61 99876-5642</td>
                                            <td>Agendado</td>
                                            <td>02-02-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Nadir Monahan</td>
                                            <td>dominic.gottlieb@example.com</td>
                                            <td>Vespertino</td>
                                            <td>61 99876-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Marcos Cox</td>
                                            <td>Junior Technical Author</td>
                                            <td>Noturno</td>
                                            <td>61 98745-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Marcelo Romero</td>
                                            <td>joaquim@gmail.com</td>
                                            <td>Matutino</td>
                                            <td>61 99876-5642</td>
                                            <td>Agendado</td>
                                            <td>02-02-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Bruno Monahan</td>
                                            <td>dominic.gottlieb@example.com</td>
                                            <td>Vespertino</td>
                                            <td>61 99876-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Reinaldo Cox</td>
                                            <td>Junior Technical Author</td>
                                            <td>Noturno</td>
                                            <td>61 98745-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Carol Romero</td>
                                            <td>joaquim@gmail.com</td>
                                            <td>Matutino</td>
                                            <td>61 99876-5642</td>
                                            <td>Agendado</td>
                                            <td>02-02-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Gabi Monahan</td>
                                            <td>dominic.gottlieb@example.com</td>
                                            <td>Vespertino</td>
                                            <td>61 99876-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Janaina Cox</td>
                                            <td>Junior Technical Author</td>
                                            <td>Noturno</td>
                                            <td>61 98745-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Jones Romero</td>
                                            <td>joaquim@gmail.com</td>
                                            <td>Matutino</td>
                                            <td>61 99876-5642</td>
                                            <td>Agendado</td>
                                            <td>02-02-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Paulão Monahan</td>
                                            <td>dominic.gottlieb@example.com</td>
                                            <td>Vespertino</td>
                                            <td>61 99876-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Miranda Cox</td>
                                            <td>Junior Technical Author</td>
                                            <td>Noturno</td>
                                            <td>61 98745-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Mario Romero</td>
                                            <td>joaquim@gmail.com</td>
                                            <td>Matutino</td>
                                            <td>61 99876-5642</td>
                                            <td>Agendado</td>
                                            <td>02-02-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Janjão Monahan</td>
                                            <td>dominic.gottlieb@example.com</td>
                                            <td>Vespertino</td>
                                            <td>61 99876-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Morena Dimas</td>
                                            <td>Junior Technical Author</td>
                                            <td>Noturno</td>
                                            <td>61 98745-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Cecília Romero</td>
                                            <td>joaquim@gmail.com</td>
                                            <td>Matutino</td>
                                            <td>61 99876-5642</td>
                                            <td>Agendado</td>
                                            <td>02-02-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Kenia Monahan</td>
                                            <td>dominic.gottlieb@example.com</td>
                                            <td>Vespertino</td>
                                            <td>61 99876-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Dalton Cox</td>
                                            <td>Junior Technical Author</td>
                                            <td>Noturno</td>
                                            <td>61 98745-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Dinho Romero</td>
                                            <td>joaquim@gmail.com</td>
                                            <td>Matutino</td>
                                            <td>61 99876-5642</td>
                                            <td>Agendado</td>
                                            <td>02-02-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Silveira Monahan</td>
                                            <td>dominic.gottlieb@example.com</td>
                                            <td>Vespertino</td>
                                            <td>61 99876-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Paulinha Cox</td>
                                            <td>Junior Technical Author</td>
                                            <td>Noturno</td>
                                            <td>61 98745-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Wanderley Romero</td>
                                            <td>joaquim@gmail.com</td>
                                            <td>Matutino</td>
                                            <td>61 99876-5642</td>
                                            <td>Agendado</td>
                                            <td>02-02-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Zulu Monahan</td>
                                            <td>dominic.gottlieb@example.com</td>
                                            <td>Vespertino</td>
                                            <td>61 99876-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Oliveira Gonçalves</td>
                                            <td>Junior Technical Author</td>
                                            <td>Noturno</td>
                                            <td>61 98745-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Luis Romero</td>
                                            <td>joaquim@gmail.com</td>
                                            <td>Matutino</td>
                                            <td>61 99876-5642</td>
                                            <td>Agendado</td>
                                            <td>02-02-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Lila Monahan</td>
                                            <td>dominic.gottlieb@example.com</td>
                                            <td>Vespertino</td>
                                            <td>61 99876-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Xenia Ashton</td>
                                            <td>Junior Technical Author</td>
                                            <td>Noturno</td>
                                            <td>61 98745-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Eliana Romero</td>
                                            <td>joaquim@gmail.com</td>
                                            <td>Matutino</td>
                                            <td>61 99876-5642</td>
                                            <td>Agendado</td>
                                            <td>02-02-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Elisa</td>
                                            <td>dominic.gottlieb@example.com</td>
                                            <td>Vespertino</td>
                                            <td>61 99876-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>João Cox</td>
                                            <td>Junior Technical Author</td>
                                            <td>Noturno</td>
                                            <td>61 98745-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Mauro Romero</td>
                                            <td>joaquim@gmail.com</td>
                                            <td>Matutino</td>
                                            <td>61 99876-5642</td>
                                            <td>Agendado</td>
                                            <td>02-02-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Maria Monahan</td>
                                            <td>dominic.gottlieb@example.com</td>
                                            <td>Vespertino</td>
                                            <td>61 99876-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Popo Cox</td>
                                            <td>Junior Technical Author</td>
                                            <td>Noturno</td>
                                            <td>61 98745-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Fabio Romero</td>
                                            <td>joaquim@gmail.com</td>
                                            <td>Matutino</td>
                                            <td>61 99876-5642</td>
                                            <td>Agendado</td>
                                            <td>02-02-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Tita Chasity Monahan</td>
                                            <td>dominic.gottlieb@example.com</td>
                                            <td>Vespertino</td>
                                            <td>61 99876-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Igor Cox</td>
                                            <td>Junior Technical Author</td>
                                            <td>Noturno</td>
                                            <td>61 98745-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Hugo Romero</td>
                                            <td>joaquim@gmail.com</td>
                                            <td>Matutino</td>
                                            <td>61 99876-5642</td>
                                            <td>Agendado</td>
                                            <td>02-02-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Oliver Chasity</td>
                                            <td>dominic.gottlieb@example.com</td>
                                            <td>Vespertino</td>
                                            <td>61 99876-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Aline Cunha</td>
                                            <td>Junior Technical Author</td>
                                            <td>Noturno</td>
                                            <td>61 98745-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions" data-intro="Esse é o painel de ações de cada registro" data-step="3">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Silvia</td>
                                            <td>joaquim@gmail.com</td>
                                            <td>Matutino</td>
                                            <td>61 99876-5642</td>
                                            <td>Agendado</td>
                                            <td>02-02-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Zumira</td>
                                            <td>dominic.gottlieb@example.com</td>
                                            <td>Vespertino</td>
                                            <td>61 99876-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Valéria Cox</td>
                                            <td>Junior Technical Author</td>
                                            <td>Noturno</td>
                                            <td>61 98745-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Cláudia Joaquim</td>
                                            <td>joaquim@gmail.com</td>
                                            <td>Matutino</td>
                                            <td>61 99876-5642</td>
                                            <td>Agendado</td>
                                            <td>02-02-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Quenia Monahan</td>
                                            <td>dominic.gottlieb@example.com</td>
                                            <td>Vespertino</td>
                                            <td>61 99876-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Paula Cox</td>
                                            <td>Junior Technical Author</td>
                                            <td>Noturno</td>
                                            <td>61 98745-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Lilian</td>
                                            <td>joaquim@gmail.com</td>
                                            <td>Matutino</td>
                                            <td>61 99876-5642</td>
                                            <td>Agendado</td>
                                            <td>02-02-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Monahan</td>
                                            <td>dominic.gottlieb@example.com</td>
                                            <td>Vespertino</td>
                                            <td>61 99876-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Diana Cox</td>
                                            <td>Junior Technical Author</td>
                                            <td>Noturno</td>
                                            <td>61 98745-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Juliana Melo</td>
                                            <td>joaquim@gmail.com</td>
                                            <td>Matutino</td>
                                            <td>61 99876-5642</td>
                                            <td>Agendado</td>
                                            <td>02-02-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Beatriz Monahan</td>
                                            <td>dominic.gottlieb@example.com</td>
                                            <td>Vespertino</td>
                                            <td>61 99876-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Vera Cox</td>
                                            <td>Junior Technical Author</td>
                                            <td>Noturno</td>
                                            <td>61 98745-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Julia Romero</td>
                                            <td>joaquim@gmail.com</td>
                                            <td>Matutino</td>
                                            <td>61 99876-5642</td>
                                            <td>Agendado</td>
                                            <td>02-02-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Gustavo Monahan</td>
                                            <td>dominic.gottlieb@example.com</td>
                                            <td>Vespertino</td>
                                            <td>61 99876-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Thiago Cox</td>
                                            <td>Junior Technical Author</td>
                                            <td>Noturno</td>
                                            <td>61 98745-5642</td>
                                            <td>Aguardando<br>agendamento</td>
                                            <td>09-07-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Flavio Romero</td>
                                            <td>joaquim@gmail.com</td>
                                            <td>Matutino</td>
                                            <td>61 99876-5642</td>
                                            <td>Agendado</td>
                                            <td>02-02-2018</td>
                                            <td class="actions">
                                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-original-title="Visualizar"><i class="icon-eye" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-warning" data-toggle="tooltip" data-original-title="Editar"><i class="icon-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-outline-danger" data-toggle="tooltip" data-original-title="Apagar"><i class="icon-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Turno para contato</th>
                                            <th>Telefone</th>
                                            <th>Status</th>
                                            <th>Último follow up</th>
                                            <th>Ações</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    
</div>

<div class="modal fade" id="NovoLead" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="largeModalLabel">Novo Lead</h4>
            </div>
            <div class="modal-body">
                <form class="form-row">
                    <div class="form-group col-md-6">
                      <label class="sr-only" for="name">Nome</label>
                      <input type="text" class="form-control" id="name" placeholder="João Cabral" required>
                    </div>
                    <div class="form-group col-md-6">
                      <label class="sr-only" for="email">Email</label>
                      <input type="email" class="form-control mb-2 mr-sm-6" id="email" placeholder="nome@servidor.com" required>
                    </div>
                    <div class="form-group col-md-4">
                      <select class="custom-select mr-sm-4" id="gender" required>
                        <option selected>Sexo</option>
                        <option value="M">Feminino</option>
                        <option value="F">Masculino</option>
                      </select>
                    </div>
                    <div class="form-group col-md-4">
                      <label class="sr-only" for="phone">Telefone:</label>
                      <input type="text" class="form-control mb-2 mr-sm-4" id="phone" placeholder="61 98765-4321" required>
                    </div>
                    <div class="form-group col-md-4">
                      <select class="custom-select mr-sm-4" id="shift_contact" required>
                        <option selected>Turno para contato:</option>
                        <option value="M">Manhã</option>
                        <option value="T">Tarde</option>
                        <option value="N">Noite</option>
                      </select>
                    </div>
                    <div class="form-group col-md-12">
                      <label class="sr-only" for="additional_notes">Notas ou observações:</label>
                      <textarea class="form-control" name="additional_notes" cols="50" rows="10" id="additional_notes"  placeholder="Notas ou observações:" required></textarea>
                    </div>
                    <div class="form-group col-md-3">
                      <label for="statusName">Status:</label>
                      <input type="text" name="statusName" value="Ativa" disabled="" class="form-control">
                    </div>
                    <div class="form-group col-sm-3">
                        <label for="date_added">Data de Criação:</label>
                        <input class="form-control" disabled="" name="created_at" type="date" value="2018-10-02">
                    </div>
                    <div class="form-group col-sm-3">
                        <label for="date_last_follow_up">Última atualização:</label>
                        <input class="form-control" disabled="" name="date_last_follow_up" type="date" value="2018-10-02" id="date_last_follow_up">
                    </div>
                    <div class="form-group col-sm-3">
                        <label for="user">Proprietário:</label>
                        <input class="form-control" name="user_id" type="hidden" value="1">
                        <input class="form-control" disabled="" name="" type="text" value="admin">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-success">SALVAR</button>
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">CANCELAR</button>
            </div>
                </form>
        </div>
    </div>
</div>

<!-- Javascript -->
<script src="assets/bundles/libscripts.bundle.js"></script>    
<script src="assets/bundles/vendorscripts.bundle.js"></script>

<script src="assets/bundles/datatablescripts.bundle.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/buttons.html5.min.js"></script>
<script src="../assets/vendor/jquery-datatable/buttons/buttons.print.min.js"></script>

<script src="../assets/vendor/sweetalert/sweetalert.min.js"></script> <!-- SweetAlert Plugin Js --> 

<script src="assets/bundles/mainscripts.bundle.js"></script>
<script src="assets/bundles/morrisscripts.bundle.js"></script>
<script src="assets/js/pages/tables/jquery-datatable.js"></script>

<script type="text/javascript">
    $(function () {
    $('.js-sweetalert').on('click', function () {
        var type = $(this).data('type');
        if (type === 'basic') {
            showBasicMessage();
        }
        else if (type === 'with-title') {
            showWithTitleMessage();
        }
        else if (type === 'success') {
            showSuccessMessage();
        }
        else if (type === 'confirm') {
            showConfirmMessage();
        }
        else if (type === 'cancel') {
            showCancelMessage();
        }
        else if (type === 'with-custom-icon') {
            showWithCustomIconMessage();
        }
        else if (type === 'html-message') {
            showHtmlMessage();
        }
        else if (type === 'autoclose-timer') {
            showAutoCloseTimerMessage();
        }
        else if (type === 'prompt') {
            showPromptMessage();
        }
        else if (type === 'ajax-loader') {
            showAjaxLoaderMessage();
        }
    });
});

//These codes takes from http://t4t5.github.io/sweetalert/
function showSuccessMessage() {
    swal("Muito bom!", "Distribuição de leads concluída com sucesso!", "success");
}

</script>

</body>
</html>
