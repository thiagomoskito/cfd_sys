<div id="rightsidebar" class="right-sidebar">
    <ul class="nav nav-tabs tab-nav-right" role="tablist">
        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#item1" aria-expanded="true">Item 1</a></li>
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#item2" aria-expanded="false">Item 2</a></li>
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#item3" aria-expanded="false">Item 3 </a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane animated fadeIn in active" id="item1" aria-expanded="true">
            <div class="sidebar-scroll">
                <div class="card">
                    <div class="header">
                        <h2>Configurações</h2>
                    </div>
                    <div class="body">
                        <ul class="setting-list list-unstyled">
                            <li>
                                <label for="checkbox1" class="fancy-checkbox">
                                    <input id="checkbox1" type="checkbox">
                                    <span>Usuários</span>
                                </label>
                            </li>
                            <li>
                                <label for="checkbox2" class="fancy-checkbox">
                                    <input id="checkbox2" type="checkbox" checked>
                                    <span>Leads</span>
                                </label>
                            </li>
                            <li>
                                <label for="checkbox3" class="fancy-checkbox">
                                    <input id="checkbox3" type="checkbox" checked>
                                    <span>Notificações</span>
                                </label>         
                            </li>
                            <li>
                                <label for="checkbox4" class="fancy-checkbox">
                                    <input id="checkbox4" type="checkbox">
                                    <span>Updates</span>
                                </label>
                            </li>
                            <li>
                                <label for="checkbox5" class="fancy-checkbox">
                                    <input id="checkbox5" type="checkbox">
                                    <span>Offline</span>
                                </label>
                            </li>
                            <li>
                                <label for="checkbox6" class="fancy-checkbox">
                                    <input id="checkbox6" type="checkbox">
                                    <span>Permissões</span>
                                </label>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane animated fadeIn" id="item2" aria-expanded="false">
            <div class="sidebar-scroll">
                <div class="card">
                    <div class="header">
                        <h2>Usuários</h2>
                    </div>
                    <div class="body">
                        <ul class="right_chat list-unstyled">
                            <li class="online">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="../assets/images/xs/avatar4.jpg" alt="">
                                        <div class="media-body">
                                            <span class="name">Chris Fox</span>
                                            <span class="message">Angular</span>
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>                            
                            </li>
                            <li class="online">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="../assets/images/xs/avatar5.jpg" alt="">
                                        <div class="media-body">
                                            <span class="name">Joge Lucky</span>
                                            <span class="message">Vendas</span>
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>                            
                            </li>
                            <li class="offline">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="../assets/images/xs/avatar2.jpg" alt="">
                                        <div class="media-body">
                                            <span class="name">Isabella</span>
                                            <span class="message">CEO</span>
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>                            
                            </li>
                            <li class="offline">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="../assets/images/xs/avatar1.jpg" alt="">
                                        <div class="media-body">
                                            <span class="name">Folisise Chosielie</span>
                                            <span class="message">PHP Expert</span>
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>                            
                            </li>
                            <li class="online">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="../assets/images/xs/avatar3.jpg" alt="">
                                        <div class="media-body">
                                            <span class="name">Alexander</span>
                                            <span class="message">Web Master</span>
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>                            
                            </li>                        
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane animated fadeIn" id="item3" aria-expanded="false">
            <div class="sidebar-scroll">
                <div class="card">
                    <div class="header">
                        <h2>Testar rolagem</h2>
                    </div>
                    <div class="body">
                        <ul class="list-unstyled basic-list">
                            <li><i class="icon-book-open m-r-5"></i> Item 1 <span class="badge badge-primary">21</span></li>
                            <li><i class="icon-list m-r-5"></i> Item 2 <span class="badge-purple badge">50</span></li>
                            <li><i class="fa fa-ticket m-r-5"></i> Item 3<span class="badge-success badge">9</span></li>
                            <li><i class="icon-tag m-r-5"></i> Item 4<span class="badge-info badge">7</span></li>
                        </ul>
                    </div>
                </div>
                <div class="card">
                    <div class="body">
                        <div class="new_timeline mt-3">
                            <div class="header">
                                <div class="color-overlay">
                                    <div class="day-number">6</div>
                                    <div class="date-right">
                                    <div class="day-name">Segunda</div>
                                    <div class="month">Agosto 2018</div>
                                    </div>
                                </div>                                
                            </div>
                            <ul>
                                <li>
                                    <div class="bullet pink"></div>
                                    <div class="time">11am</div>
                                    <div class="desc">
                                        <h3>Atendimento</h3>
                                        <h4>Visitar cliente</h4>
                                    </div>
                                </li>
                                <li>
                                    <div class="bullet green"></div>
                                    <div class="time">12pm</div>
                                    <div class="desc">
                                        <h3>Desenvolvedores</h3>
                                        <h4>Hangouts</h4>
                                        <ul class="list-unstyled team-info margin-0 p-t-5">                                            
                                            <li><img src="../assets/images/xs/avatar1.jpg" alt="Avatar"></li>
                                            <li><img src="../assets/images/xs/avatar2.jpg" alt="Avatar"></li>
                                            <li><img src="../assets/images/xs/avatar3.jpg" alt="Avatar"></li>
                                            <li><img src="../assets/images/xs/avatar4.jpg" alt="Avatar"></li>                                            
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <div class="bullet orange"></div>
                                    <div class="time">3:30pm</div>
                                    <div class="desc">
                                        <h3>Café da tarde</h3>
                                    </div>
                                </li>
                                <li>
                                    <div class="bullet green"></div>
                                    <div class="time">2pm</div>
                                    <div class="desc">
                                        <h3>Entrega</h3>
                                        <h4>Partiu casa!</h4>
                                    </div>
                                </li>
                                <li>
                                    <div class="bullet green"></div>
                                    <div class="time">6:30pm</div>
                                    <div class="desc">
                                        <h3>Happy hour</h3>
                                    </div>
                                </li>
                            </ul>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>