            <div class="navbar-right">
                <ul class="list-unstyled clearfix mb-0">
                    <li>
                        <div class="navbar-btn btn-toggle-show">
                            <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
                        </div>                        
                        <a href="javascript:void(0);" class="btn-toggle-fullwidth btn-toggle-hide"><i class="fa fa-bars"></i></a>
                    </li>
                    <li>
                        <div id="navbar-menu">
                            <ul class="nav navbar-nav">

                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle icon-menu rocket" data-toggle="dropdown" data-toggle="tooltip" data-original-title="Início rápido">
                                        <i class="icon-rocket"></i>
                                    </a>
                                    <ul class="dropdown-menu animated bounceIn notifications">
                                        <li class="header"><strong>Início rápido!</strong></li>
                                        <li class="icones col-6">
                                            <a href="javascript:void(0);">
                                                <i class="icon-book-open"></i>
                                            </a>
                                            <span>Leads</span>
                                        </li>
                                        <li class="icones col-6">
                                            <a href="javascript:void(0);">
                                                <i class="icon-calendar"></i>
                                                
                                            </a>
                                            <span>Agenda</span>
                                        </li>

                                        <li class="icones col-6">
                                            <a href="javascript:void(0);">
                                                <i class="icon-calendar"></i>
                                                
                                            </a>
                                            <span>Outro</span>
                                        </li>

                                        <li class="icones col-6">
                                            <a href="javascript:void(0);">
                                                <i class="icon-calendar"></i>
                                                
                                            </a>
                                            <span>Um outro</span>
                                        </li>

                                        
                                    </ul>
                                </li>



                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                                        <img class="rounded-circle" src="../assets/images/user-small.png" width="30" alt="">
                                    </a>
                                    <div class="dropdown-menu animated flipInY user-profile">
                                        <div class="d-flex p-3 align-items-center">
                                            <div class="drop-left m-r-10">
                                                <img src="../assets/images/user-small.png" class="rounded" width="50" alt="">
                                            </div>
                                            <div class="drop-right">
                                                <h4>Rose Nascimento</h4>
                                                <p class="user-name">rose.nascimento</p>
                                            </div>
                                        </div>
                                        <div class="m-t-10 p-3 drop-list">
                                            <ul class="list-unstyled">
                                                <li><a href="#"><i class="icon-user"></i>Perfil</a></li>
                                                <li><a href="javascript:void(0);"><i class="icon-pencil"></i>Editar perfil</a></li>
                                                <li class="divider"></li>
                                                <li><a href="index.html"><i class="icon-power"></i>Sair</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <a class="icon-menu" href="javascript:void(0);" onclick="introJs().start()" data-toggle="tooltip" data-original-title="Quer ajuda?"><i class="icon-support"></i></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="icon-menu conf js-right-sidebar" data-toggle="tooltip" data-original-title="Configurações"><i class="icon-settings"></i></a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>