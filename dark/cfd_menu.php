    <div id="leftsidebar" class="sidebar">
        <div class="sidebar-scroll">
            <nav id="leftsidebar-nav" class="sidebar-nav">
                <ul id="main-menu" class="metismenu">
                    <li class="active"><a href="cfd_painel.php"><i class="icon-home"></i><span>Painel de Controle</span></a></li>
                    <li class="heading">Administração</li>
                    <li><a href="cfd_arvore.php"><i class="fa fa-tree"></i><span>Árvore</span></a></li>
                    <li><a href="cfd_leads.php"><i class="icon-wallet"></i><span>Leads</span></a></li>
                    <li class="heading">Menu</li>                   
                    <li class="top">
                        <a href="#menu1" class="has-arrow"><i class="icon-energy"></i> <span>Menu 1</span></a>
                        <ul>
                            <li><a href="#">Menu 1.1</a></li>
                            <li><a href="#">Menu 1.2</a></li>
                            <li><a href="#">Menu 1.3</a></li>
                        </ul>
                    </li>
                    <li class="top">
                        <a href="#menu2" class="has-arrow"><i class="icon-key"></i> <span>Menu 2</span></a>
                        <ul>
                            <li><a href="#">Menu 2.1</a></li>
                            <li><a href="#">Menu 2.2</a></li>
                            <li><a href="#">Menu 2.3</a></li>
                        </ul>
                    </li>
                    <li class="top">
                        <a href="#menu3" class="has-arrow"><i class="icon-key"></i> <span>Menu 3</span></a>
                        <ul>
                            <li><a href="#">Menu 3.1</a></li>
                            <li><a href="#">Menu 3.2</a></li>
                            <li><a href="#">Menu 3.3</a></li>
                        </ul>
                    </li>
                    <li class="top">
                        <a href="#menu4" class="has-arrow"><i class="icon-key"></i> <span>Menu 4</span></a>
                        <ul>
                            <li><a href="#">Menu 4.1</a></li>
                            <li><a href="#">Menu 4.2</a></li>
                            <li><a href="#">Menu 4.3</a></li>
                        </ul>
                    </li>
                    <li class="top">
                        <a href="#menu5" class="has-arrow"><i class="icon-key"></i> <span>Menu 5</span></a>
                        <ul>
                            <li><a href="#">Menu 5.1</a></li>
                            <li><a href="#">Menu 5.2</a></li>
                            <li><a href="#">Menu 5.3</a></li>
                        </ul>
                    </li>
                    <li class="top">
                        <a href="#menu6" class="has-arrow"><i class="icon-key"></i> <span>Menu 6</span></a>
                        <ul>
                            <li><a href="#">Menu 6.1</a></li>
                            <li><a href="#">Menu 6.2</a></li>
                            <li><a href="#">Menu 6.3</a></li>
                        </ul>
                    </li>
                    <li class="top">
                        <a href="#menu7" class="has-arrow"><i class="icon-key"></i> <span>Menu 7</span></a>
                        <ul>
                            <li><a href="#">Menu 7.1</a></li>
                            <li><a href="#">Menu 7.2</a></li>
                            <li><a href="#">Menu 7.3</a></li>
                        </ul>
                    </li>
                    <li class="top">
                        <a href="#menu8" class="has-arrow"><i class="icon-key"></i> <span>Menu 8</span></a>
                        <ul>
                            <li><a href="#">Menu 8.1</a></li>
                            <li><a href="#">Menu 8.2</a></li>
                            <li><a href="#">Menu 8.3</a></li>
                        </ul>
                    </li>
                    <li class="heading">
                        <span>
                        © Todos direitos reservados 2018 <br>
                        Versão 1.0.0
                        </span>
                    </li>
                </ul>
            </nav>
        </div>
    </div>

    <?php
    include('cfd_confg.php');
    ?>