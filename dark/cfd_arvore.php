<!doctype html>
<html lang="en">

<head>
<title>CFD Business - Árvore</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="Sys Template">
<meta name="author" content="A42, design by: Thiago Moskito">

<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../assets/vendor/animate-css/animate.min.css">
<link rel="stylesheet" href="../assets/vendor/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="../assets/vendor/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css">
<link rel="stylesheet" href="../assets/vendor/chartist/css/chartist.min.css">
<link rel="stylesheet" href="../assets/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">

<!-- MAIN CSS -->
<link rel="stylesheet" href="assets/css/main.css">
<link rel="stylesheet" href="assets/css/cfd_skins.css">
</head>
<body class="theme-cfd">

<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img src="../assets/images/thumbnail_dark.png" width="50" height="50" alt="CFD Business"></div>
        <p>Carregando...</p>
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay" style="display: none;"></div>

<div id="wrapper">

    <nav class="navbar navbar-fixed-top">
        <div class="container-fluid">

            <div class="navbar-brand">
                <a href="cfd_painel.html">
                    <img src="../assets/images/logo-icon.svg" alt="Logo" class="img-responsive logo">
                    <span class="name"><img src="../assets/images/lg_60.png" alt="Logo"></span>
                </a>
            </div>
            
            <?php
            include('cfd_top.php');
            ?>

        </div>
    </nav>

    <?php
    include('cfd_menu.php');
    ?>

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">
                        <h2>Árvore <span>Visualização de estrutura de dados</span></h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="cfd_painel.php"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item active">Árvore</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">

                    <div id="tree-simple" style="width: 100%; height: 100%" class="Treant Treant-loaded"><svg height="500" version="1.1" width="1016" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: relative;"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.4</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path fill="none" stroke="#000000" d="M442.75,88.5L442.75,103.5L332.75,103.5L332.75,118.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="none" stroke="#000000" d="M332.75,188.5L332.75,203.5L332.75,203.5L332.75,218.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="none" stroke="#000000" d="M442.75,88.5L442.75,103.5L548.25,103.5L548.25,118.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="none" stroke="#000000" d="M548.25,188.5L548.25,203.5L548.25,203.5L548.25,218.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="none" stroke="#000000" d="M548.25,288.5L548.25,303.5L438.75,303.5L438.75,318.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="none" stroke="#000000" d="M548.25,288.5L548.25,303.5L662.25,303.5L662.25,318.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="none" stroke="#000000" d="M662.25,388.5L662.25,403.5L662.25,403.5L662.25,418.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path></svg><div class="node nodeExample1" style="left: 584.25px; top: 418.5px;"><p class="node-name">Passo 1</p><p class="node-title">(#8) - [tipo:D] - Paulo2</p></div><div class="node nodeExample1" style="left: 562.75px; top: 318.5px;"><p class="node-name">Passo 1</p><p class="node-title">(#7) - [tipo:C] - João Mateus2</p><a class="collapse-switch"></a></div><div class="node nodeExample1" style="left: 334.75px; top: 318.5px;"><p class="node-name">Passo 1</p><p class="node-title">(#6) - [tipo:B] - Pedro Melucci2</p></div><div class="node nodeExample1" style="left: 481.75px; top: 218.5px;"><p class="node-name">Passo 1</p><p class="node-title">(#5) - [tipo:A] - Rui</p><a class="collapse-switch"></a></div><div class="node nodeExample1" style="left: 448.25px; top: 118.5px;"><p class="node-name">FOCO</p><p class="node-title">(#3) - [tipo:B] - Pedro Melucci</p><a class="collapse-switch"></a></div><div class="node nodeExample1" style="left: 258.75px; top: 218.5px;"><p class="node-name">FOCO</p><p class="node-title">(#4) - [tipo:D] - Paulo</p></div><div class="node nodeExample1" style="left: 237.25px; top: 118.5px;"><p class="node-name">FOCO</p><p class="node-title">(#2) - [tipo:C] - João Mateus</p><a class="collapse-switch"></a></div><div class="node nodeExample1" style="left: 347.25px; top: 18.5px;"><p class="node-name">FOCO</p><p class="node-title">(#1) - [tipo:A] - Walter Hugo</p><a class="collapse-switch"></a></div></div>

                </div>
            </div>

        </div>
    </div>
    
</div>

<!-- Javascript -->
<script src="assets/bundles/libscripts.bundle.js"></script>    
<script src="assets/bundles/vendorscripts.bundle.js"></script>

<script src="assets/bundles/chartist.bundle.js"></script>
<script src="assets/bundles/knob.bundle.js"></script> <!-- Jquery Knob-->
<script src="assets/bundles/flotscripts.bundle.js"></script> <!-- flot charts Plugin Js --> 
<script src="../assets/vendor/flot-charts/jquery.flot.selection.js"></script>

<script src="assets/bundles/mainscripts.bundle.js"></script>
<script src="assets/js/index.js"></script>
</body>
</html>
