<!doctype html>
<html lang="en">

<head>
<title>CFD Business - Painel</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="Sys Template">
<meta name="author" content="A42, design by: Thiago Moskito">

<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../assets/vendor/animate-css/animate.min.css">
<link rel="stylesheet" href="../assets/vendor/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="../assets/vendor/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css">
<link rel="stylesheet" href="../assets/vendor/chartist/css/chartist.min.css">
<link rel="stylesheet" href="../assets/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">

<link rel="stylesheet" href="assets/css/introjs.css">

<!-- MAIN CSS -->
<link rel="stylesheet" href="assets/css/main.css">
<link rel="stylesheet" href="assets/css/cfd_skins.css">

<script src="intro.js"></script>
<script type="text/javascript">
  var autostartMatch = window.location.search.match(/(?:\?|\&)autostart=([^\&]*)(?:\&|$)/);
  if (autostartMatch && autostartMatch[1]) {
    jQuery(function() {
      introJs().start();
    })
  }
</script>
</head>
<body class="theme-cfd">

<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img src="../assets/images/thumbnail_dark.png" width="50" height="50" alt="CFD Business"></div>
        <p>Carregando...</p>
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay" style="display: none;"></div>

<div id="wrapper">

    <nav class="navbar navbar-fixed-top">
        <div class="container-fluid">

            <div class="navbar-brand">
                <a href="cfd_painel.html">
                    <img src="../assets/images/logo-icon.svg" alt="Logo" class="img-responsive logo">
                    <span class="name"><img src="../assets/images/lg_60.png" alt="Logo"></span>
                </a>
            </div>
            
            <?php
            include('cfd_top.php');
            ?>

        </div>
    </nav>

    <?php
    include('cfd_menu.php');
    ?>

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">
                        <h2 data-intro="Aqui fica o título da página atual." data-step="1">Painel <span>página principal</span></h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="cfd_painel.php"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item active" data-intro="Você está aqui." data-step="2">Painel</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row clearfix">

                    <div class="col-lg-8 col-md-12">
                        <div class="card">
                            <div class="header">
                                <h2>Agenda <small>Leads agendados</small></h2>
                            </div>
                            <div class="body">                            
                                <ul class="list-unstyled feeds_widget">
                                    <li>
                                        <div class="feeds-left"><i class="fa fa-user"></i></div>
                                        <div class="feeds-body">
                                            <h4 class="title">Neil Schmitt <small class="float-right text-muted">Hoje</small></h4>
                                            <small>Turno para contato: Matutino</small>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="feeds-left"><i class="fa fa-user"></i></div>
                                        <div class="feeds-body">
                                            <h4 class="title">Marcos Huels <small class="float-right text-muted">Hoje</small></h4>
                                            <small>Turno para contato: Matutino</small>
                                        </div>
                                    </li>     
                                    <li>
                                        <div class="feeds-left"><i class="fa fa-user"></i></div>
                                        <div class="feeds-body">
                                            <h4 class="title">Lea Reinger <small class="float-right text-muted">21/10/18</small></h4>
                                            <small>Turno para contato: Noturno</small>
                                        </div>
                                    </li>                               
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">

                        <div class="card">
                            <div class="header">
                                <h2>Leads</h2>
                            </div>
                            <div class="body">
                                <ul class=" list-unstyled basic-list">
                                    <li><i class="icon-book-open m-r-5"></i> Total <span class="badge badge-primary">91</span></li>
                                    <li><i class="icon-user-follow m-r-5"></i> Cadastrados<span class="badge-info badge">7</span></li>
                                    <li><i class="icon-call-in m-r-5"></i> Contactados <span class="badge-purple badge">50</span></li>
                                    <li><i class="icon-call-end m-r-5"></i> Não contactados <span class="badge-danger badge">37</span></li>
                                    <li><i class="icon-user-following m-r-5"></i> Convertidos<span class="badge-success badge">9</span></li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>

            </div>

        </div>
    </div>
    
</div>

<!-- Javascript -->
<script src="assets/bundles/libscripts.bundle.js"></script>    
<script src="assets/bundles/vendorscripts.bundle.js"></script>

<script src="assets/bundles/chartist.bundle.js"></script>
<script src="assets/bundles/knob.bundle.js"></script> <!-- Jquery Knob-->
<script src="assets/bundles/flotscripts.bundle.js"></script> <!-- flot charts Plugin Js --> 
<script src="../assets/vendor/flot-charts/jquery.flot.selection.js"></script>

<script src="assets/bundles/mainscripts.bundle.js"></script>
<script src="assets/js/index.js"></script>
</body>
</html>
